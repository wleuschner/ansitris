/*    This file is part of ANSITRIS.

    ANSITRIS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ANSITRIS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ANSITRIS.  If not, see <http://www.gnu.org/licenses/>.*/
#include "Console.h"
#include<cstring>
using namespace std;

Console *Console::c=0;

Console::Console()
{
	setMode(80,50);
}

void Console::setMode(int x,int y,char*title)
{
	width=x;
	height=y;
	rHnd=GetStdHandle(STD_INPUT_HANDLE);
	wHnd=GetStdHandle(STD_OUTPUT_HANDLE);
	SMALL_RECT size = {0,0,width-1,height-1};
	SetConsoleWindowInfo(wHnd,TRUE,&size);
	COORD bSize = {width,height};
	SetConsoleScreenBufferSize(wHnd,bSize);
	buffer=new CHAR_INFO[width*height];
	setConsoleTitle(title);
}

Console::~Console()
{
	if(c!=0)
	{
		delete buffer;
	}
}

void Console::setConsoleTitle(char*title)
{
	SetConsoleTitle(title);
}

char *Console::getConsoleTitle()
{
	static char title[100];
	GetConsoleTitle(title,100);
	return title;
}

void Console::draw()
{
	COORD bsize={width,height};
	COORD cPos={0,0};
	SMALL_RECT wArea={0,0,width-1,height-1};
	WriteConsoleOutput(wHnd,buffer,bsize,cPos,&wArea);
}

void Console::putChar(int x,int y,char c,int color)
{
	CHAR_INFO ci;
	ci.Char.AsciiChar=c;
	ci.Attributes=color;
	buffer[y*width+x]=ci;
}

bool Console::isPushed(int keyCode)
{
	return keys[keyCode];
}

void Console::poll()
{
	memset(keys,0,sizeof(bool)*128);
	DWORD numEvents=0;
	DWORD numEventsRead=0;
	GetNumberOfConsoleInputEvents(rHnd,&numEvents);
	if(numEvents!=0)
	{
		INPUT_RECORD *eventBuffer = new INPUT_RECORD[numEvents];
		ReadConsoleInput(rHnd,eventBuffer,numEvents,&numEventsRead);
		for(int i=0;i<numEventsRead;i++)
		{
			if(eventBuffer[i].EventType==KEY_EVENT)
			{
				keys[eventBuffer[i].Event.KeyEvent.wVirtualKeyCode]=eventBuffer[i].Event.KeyEvent.bKeyDown;
			}
		}
		delete eventBuffer;
	}
}

int Console::getColorxy(int x,int y)
{
	return buffer[y*width+x].Attributes;
}

void Console::putString(int x,int y,char *s,int color)
{
	for(int i=0;i<strlen(s);i++)
	{
		putChar(x+i,y,s[i],color);
	}
}

void Console::clrscr(char c,int color)
{
	for(int i=0;i<width*height;i++)
	{
		buffer[i].Attributes=color;
		buffer[i].Char.AsciiChar=c;
	}
}

void Console::activateCursor(bool c)
{
	CONSOLE_CURSOR_INFO cci;
	GetConsoleCursorInfo(wHnd,&cci);
	cci.bVisible=c;
	SetConsoleCursorInfo(wHnd,&cci);
}

int Console::getWidth()
{
	return width;
}

int Console::getHeight()
{
	return height;
}

Console *Console::getInstance()
{
	if(c==0)
	{
		c=new Console();
	}
	return c;
}

void Console::destroy()
{
	delete c;
	c=0;
}
