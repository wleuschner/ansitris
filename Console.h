/*    This file is part of ANSITRIS.

    ANSITRIS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ANSITRIS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ANSITRIS.  If not, see <http://www.gnu.org/licenses/>.*/
#pragma once
#include <windows.h>

class Console
{
public:
	static Console *getInstance();
	static enum
	{
		FG_RED=FOREGROUND_RED,
		FG_BLUE=FOREGROUND_BLUE,
		FG_GREEN=FOREGROUND_GREEN,
		FG_BRIGHTER=FOREGROUND_INTENSITY,
		BG_RED=BACKGROUND_RED,
		BG_BLUE=BACKGROUND_BLUE,
		BG_GREEN=BACKGROUND_GREEN,
		BG_BRIGHTER=BACKGROUND_INTENSITY
	};
	~Console();
	void setConsoleTitle(char*title);
	char *getConsoleTitle();
	void draw();
	void putChar(int x,int y,char c,int color);
	void putString(int x,int y,char *s,int color);
	void activateCursor(bool c);
	int getWidth();
	int getHeight();
	void setMode(int x,int y,char*title=0);
	static void destroy();
	void clrscr(char c,int color);
	int getColorxy(int x,int y);
	void poll();
	bool isPushed(int keyCode);
private:
	static Console *c;
	Console();
	HANDLE rHnd,wHnd;
	CHAR_INFO *buffer;
	bool keys[128];
	int width,height;
};