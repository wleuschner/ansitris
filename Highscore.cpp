/*    This file is part of ANSITRIS.

    ANSITRIS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ANSITRIS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ANSITRIS.  If not, see <http://www.gnu.org/licenses/>.*/
#include "Highscore.h"

int Score::getScore()
{
	return score;
}

char* Score::getName()
{
	return name;
}

void Score::setScore(int s)
{
	score=s;
}

void Score::setName(char* n)
{
	strcpy(name,n);
}

Highscore::Highscore(int m)
{
	setMax(m);
	numScores=0;
}

Highscore::~Highscore(void)
{
}
//Error
void Highscore::addScore(Score s)
{
	vector<Score>::iterator i=scores.begin();
	for(;i!=scores.end();i++)
	{
		if(i->getScore()<s.getScore())
		{
			break;
		}
	}
	if(i!=scores.end())
	{
		scores.insert(i,s);
	}
	else if(numScores<10)
	{
		scores[numScores]=s;
	}
	numScores++;
}

Score Highscore::getScore(int i)
{
	return scores[i];
}

int Highscore::getMax()
{
	return max;
}

void Highscore::setMax(int m)
{
	max=m;
	scores.resize(m);
	scores.reserve(m);
}

int Highscore::getNumScores()
{
	return numScores;
}
