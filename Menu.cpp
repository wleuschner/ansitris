/*    This file is part of ANSITRIS.

    ANSITRIS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ANSITRIS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ANSITRIS.  If not, see <http://www.gnu.org/licenses/>.*/
#include "Menu.h"

Menu::Menu(void)
{
	elements.resize(16);
	selected=0;
	maxElements=0;
}

Menu::~Menu(void)
{
}

void Menu::addItem(MenuItem* item)
{
	elements[maxElements]=item;
	maxElements++;
}

char* Menu::getTitle(int id)
{
	return elements[id]->getTitle();
}

void Menu::callBack(int id)
{
	elements[id]->function();
}

int Menu::getSelected()
{
	return selected;
}

int Menu::getMaxElements()
{
	return maxElements;
}

void Menu::move(int s)
{
	selected+=s;
	if(selected<0)
	{
		selected=maxElements-1;
	}
	else if(selected>=maxElements)
	{
		selected=0;
	}
}
