/*    This file is part of ANSITRIS.

    ANSITRIS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ANSITRIS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ANSITRIS.  If not, see <http://www.gnu.org/licenses/>.*/
#include "MenuItem.h"
#include <cstring>
#include <boost/bind.hpp>
#include <boost/ref.hpp>

MenuItem::MenuItem(void)
{
}

MenuItem::~MenuItem(void)
{
}

void MenuItem::setTitle(char*t)
{
	strcpy(title,t);
}

void MenuItem::function()
{
	callback();
}

char* MenuItem::getTitle()
{
	return title;
}
