/*    This file is part of ANSITRIS.

    ANSITRIS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ANSITRIS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ANSITRIS.  If not, see <http://www.gnu.org/licenses/>.*/
#include "TetrisControl.h"
#include <cstring>
using namespace std;


TetrisControl::TetrisControl(void)
{
	ttime=1000;
	loose=false;
	curStone=model.getCurStone();
	nextStone=model.getNextStone();
	field=model.getField();
	run=0;
}

void TetrisControl::timerProc()
{
	static int start=GetTickCount()+ttime;
	if(run)
	{
		start=GetTickCount()+ttime;
		run=0;
	}
	if(GetTickCount()>=start)
	{
		start+=ttime;
		move(0,1);
	}
}

void TetrisControl::removeLines()
{
	int lines=0;
	for(int y=0;y<model.FIELD_HEIGHT;y++)
	{
		int count=0;
		for(int x=0;x<model.FIELD_WIDTH;x++)
		{
			if(field[y*model.FIELD_WIDTH+x])
			{
				count++;
			}
		}
		if(count==model.FIELD_WIDTH-1)
		{
			for(int x=0;x<model.FIELD_WIDTH;x++)
			{
				field[y*model.FIELD_WIDTH+x]=0;
			}
			for(int i=y;i>0;i--)
			{
				for(int x=0;x<model.FIELD_WIDTH;x++)
				{
					field[i*model.FIELD_WIDTH+x]=field[(i-1)*model.FIELD_WIDTH+x];
				}
			}
			lines++;
			y--;
		}
	}
	addLines(lines);
	addPoints(lines*10*model.getLevel());
}

TetrisControl::~TetrisControl(void)
{
}

void TetrisControl::rotate()
{
	int tmp[model.STONE_WIDTH*model.STONE_HEIGHT];
	memcpy(tmp,curStone,sizeof(int)*model.STONE_HEIGHT*model.STONE_WIDTH);
	for(int x=0;x<model.STONE_WIDTH;x++)
	{
		for(int y=0;y<model.STONE_HEIGHT;y++)
		{
			curStone[y*model.STONE_WIDTH+x]=tmp[x*model.STONE_WIDTH+((model.STONE_HEIGHT-1)-y)];
		}
	}
	if(checkCollision())
	{
		memcpy(curStone,tmp,sizeof(int)*model.STONE_HEIGHT*model.STONE_WIDTH);
	}
}

void TetrisControl::move(int x,int y)
{
	int curX=model.getCurX()+x;
	int curY=model.getCurY()+y;
	model.setCurX(curX);
	model.setCurY(curY);
	if(checkCollision())
	{
		curX-=x;
		curY-=y;
		model.setCurX(curX);
		model.setCurY(curY);
		if(y!=0)
		{
			ankerStone();
		}
	}
	addPoints(y*getLevel());
}

int* TetrisControl::getField()
{
	return field;
}

int* TetrisControl::getCurStone()
{
	return curStone;
}

int* TetrisControl::getNextStone()
{
	return nextStone;
}

int TetrisControl::getCurX()
{
	return model.getCurX();
}

int TetrisControl::getCurY()
{
	return model.getCurY();
}

void TetrisControl::ankerStone()
{
	int cx=model.getCurX();
	int cy=model.getCurY();
	int color=model.getCurcolor();
	for(int x=0;x<model.STONE_WIDTH;x++)
	{
		for(int y=0;y<model.STONE_HEIGHT;y++)
		{
			if(cx+x>0 && cx+x<model.FIELD_WIDTH && cy+y>0 && cy+y<model.FIELD_HEIGHT && curStone[y*model.STONE_WIDTH+x])
			{
				field[(y+cy)*model.FIELD_WIDTH+(x+cx)]|=color;
			}
		}
	}
	model.nextStones();
	loose=checkLose();
	removeLines();
}

bool TetrisControl::checkCollision()
{
	int cx=model.getCurX();
	int cy=model.getCurY();
	for(int x=0;x<model.STONE_WIDTH;x++)
	{
		for(int y=0;y<model.STONE_HEIGHT;y++)
		{
			if(curStone[y*model.STONE_WIDTH+x]&&field[(y+cy)*model.FIELD_WIDTH+(cx+x)])
			{
				return true;
			}
			if(((cx+x>=model.FIELD_WIDTH || cy+y>=model.FIELD_HEIGHT || cx+x<=0) && curStone[y*model.STONE_WIDTH+x]!=0))
			{
				return true;
			}
		}
	}
	return false;
}

void TetrisControl::newGame()
{
	setLoose(false);
	model.clean();
	model.nextStones();
	run=1;
	ttime=1000;
}

bool TetrisControl::checkLose()
{
	for(int i=0;i<model.FIELD_WIDTH;i++)
	{
		if(field[model.FIELD_WIDTH+i])
		{
			return true;
		}
	}
	return false;
}

int TetrisControl::getPoints()
{
	return model.getPoints();
}

void TetrisControl::addPoints(int p)
{
	model.setPoints(model.getPoints()+p);
	if(getPoints()/(100*getLevel()*getLevel())!=0)
	{
		addLevel(1);
		ttime-=10*getLevel();
	}
}

void TetrisControl::setLoose(bool l)
{
	loose=l;
}

bool TetrisControl::getLoose()
{
	return loose;
}

void TetrisControl::addLines(int l)
{
	model.setLines(model.getLines()+l);
}

void TetrisControl::addLevel(int l)
{
	model.setLevel(model.getLevel()+l);
}

int TetrisControl::getLines()
{
	return model.getLines();
}

int TetrisControl::getLevel()
{
	return model.getLevel();
}

void TetrisControl::resume()
{
	run=1;
}

void TetrisControl::stop()
{
	run=0;
}

int TetrisControl::getRun()
{
	return run;
}

int TetrisControl::getCurColor()
{
	return model.getCurcolor();
}

int TetrisControl::getNextColor()
{
	return model.getNextColor();
}

