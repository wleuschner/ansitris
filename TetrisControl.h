/*    This file is part of ANSITRIS.

    ANSITRIS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ANSITRIS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ANSITRIS.  If not, see <http://www.gnu.org/licenses/>.*/
#pragma once
#include <windows.h>
#include "TetrisModel.h"

class TetrisControl
{
public:
	TetrisControl(void);
	~TetrisControl(void);
	void rotate();
	void move(int x,int y);
	int getPoints();
	int getLines();
	int getLevel();
	void addPoints(int p);
	void addLines(int l);
	void addLevel(int l);
	int* getField();
	int* getCurStone();
	int* getNextStone();
	int getCurX();
	int getCurY();
	int getCurColor();
	int getNextColor();
	void ankerStone();
	void setLoose(bool l);
	bool getLoose();
	void newGame();
	void timerProc();
	void resume();
	void stop();
	int getRun();
private:
	int run;
	int ttime;
	bool loose;
	bool checkCollision();
	bool checkLose();
	void removeLines();
	int *field;
	int *curStone;
	int *nextStone;
	TetrisModel model;
};
