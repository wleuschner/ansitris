/*    This file is part of ANSITRIS.

    ANSITRIS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ANSITRIS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ANSITRIS.  If not, see <http://www.gnu.org/licenses/>.*/
#include "TetrisModel.h"
#include <cstring>
#include <ctime>
#include <cstdlib>
using namespace std;

TetrisModel::TetrisModel(void)
{
	int tmp[7][STONE_WIDTH*STONE_HEIGHT]={{0,1,0,0,
										  0,1,0,0,
										  0,1,0,0,
										  0,1,0,0},
	
										 {0,0,0,0,
										  0,1,1,0,
										  0,1,1,0,
										  0,0,0,0},
	
										 {0,1,0,0,
										  0,1,1,0,
										  0,0,1,0,
										  0,0,0,0},
	
										 {0,1,0,0,
										  1,1,0,0,
										  1,0,0,0,
										  0,0,0,0},
	
										 {0,1,0,0,
										  0,1,0,0,
										  0,1,1,0,
										  0,0,0,0},
	
										 {0,1,0,0,
										  0,1,0,0,
										  1,1,0,0,
										  0,0,0,0},
	
										 {0,1,0,0,
										  0,1,1,0,
										  0,1,0,0,
										  0,0,0,0}};
	for(int i=0;i<7;i++)
	{
		memcpy(stones[i],tmp[i],sizeof(int)*STONE_WIDTH*STONE_HEIGHT);
	}
	srand(time(0));
	clean();
	nextStones();
	nextStones();
}

TetrisModel::~TetrisModel(void)
{
}

void TetrisModel::setCurX(int x)
{
	curX=x;
}

void TetrisModel::setCurY(int y)
{
	curY=y;
}

void TetrisModel::setField(int *f)
{
	memcpy(field,f,sizeof(int)*FIELD_WIDTH*FIELD_HEIGHT);
}

void TetrisModel::setCurStone(int *s)
{
	memcpy(curStone,s,sizeof(int)*STONE_WIDTH*STONE_HEIGHT);
}

void TetrisModel::setNextStone(int *s)
{
	memcpy(nextStone,s,sizeof(int)*STONE_WIDTH*STONE_HEIGHT);
}

int TetrisModel::getCurX()
{
	return curX;
}

int TetrisModel::getCurY()
{
	return curY;
}

int* TetrisModel::getField()
{
	return field;
}

int* TetrisModel::getCurStone()
{
	return curStone;
}

int* TetrisModel::getNextStone()
{
	return nextStone;
}

void TetrisModel::clean()
{
	memset(field,0,sizeof(int)*FIELD_WIDTH*FIELD_HEIGHT);
	points=0;
	level=1;
	lines=0;
}

void TetrisModel::nextStones()
{
	curColor=nextColor;
	nextColor=(rand()%8)+1;
	memcpy(curStone,nextStone,sizeof(int)*STONE_HEIGHT*STONE_WIDTH);
	memcpy(nextStone,stones[rand()%7],sizeof(int)*STONE_HEIGHT*STONE_WIDTH);
	curY=0;
	curX=FIELD_WIDTH/2;
}

void TetrisModel::setPoints(int p)
{
	points=p;
}

int TetrisModel::getPoints()
{
	return points;
}

void TetrisModel::setLines(int l)
{
	lines=l;
}

void TetrisModel::setLevel(int l)
{
	level=l;
}

int TetrisModel::getLines()
{
	return lines;
}

int TetrisModel::getLevel()
{
	return level;
}

int TetrisModel::getCurcolor()
{
	return curColor;
}

int TetrisModel::getNextColor()
{
	return nextColor;
}
