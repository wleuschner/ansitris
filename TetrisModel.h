/*    This file is part of ANSITRIS.

    ANSITRIS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ANSITRIS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ANSITRIS.  If not, see <http://www.gnu.org/licenses/>.*/
#pragma once

class TetrisModel
{
public:
	enum
	{
		FIELD_WIDTH=10,
		FIELD_HEIGHT=19,
		STONE_WIDTH=4,
		STONE_HEIGHT=4
	};
	TetrisModel(void);
	~TetrisModel(void);
	void setCurX(int x);
	void setCurY(int y);
	void setField(int *f);
	void setCurStone(int *s);
	void setNextStone(int *s);
	void setPoints(int p);
	void setLines(int l);
	void setLevel(int l);

	int getCurX();
	int getCurY();
	int* getField();
	int* getCurStone();
	int* getNextStone();
	int getPoints();
	int getLines();
	int getLevel();
	int getCurcolor();
	int getNextColor();

	void clean();
	void nextStones();
private:
	int nextColor;
	int curColor;
	int points;
	int lines;
	int level;
	int field[FIELD_WIDTH*FIELD_HEIGHT];
	int curX,curY;
	int curStone[STONE_WIDTH*STONE_HEIGHT];
	int nextStone[STONE_WIDTH*STONE_HEIGHT];
	int stones[7][STONE_WIDTH*STONE_HEIGHT];
};
