/*    This file is part of ANSITRIS.

    ANSITRIS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ANSITRIS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ANSITRIS.  If not, see <http://www.gnu.org/licenses/>.*/
#include "TetrisView.h"
#include "TetrisModel.h"
#include "Console.h"
#include <conio.h>
#include <iostream>
#include <windows.h>
#include <boost/bind.hpp>
#include <boost/ref.hpp>
#include <fstream>
using namespace std;

TetrisView::TetrisView(void)
{
	createMenu();
	c=Console::getInstance();
	c->activateCursor(false);
	c->setMode(50,30);
	curStone = tc.getCurStone();
	nextStone = tc.getNextStone();
	field = tc.getField();
	run=false;
	done=0;
	score=new Highscore(10);
	readScores();
	initSound();
	song=2;
	selectedSong=0;
	change=true;
}

TetrisView::~TetrisView(void)
{
	writeScores();
	Console::destroy();
	deinitSound();
}

void TetrisView::createMenu()
{
	SongA.setCallBack(&TetrisView::selectSongA,this);
	SongB.setCallBack(&TetrisView::selectSongB,this);
	SongC.setCallBack(&TetrisView::selectSongC,this);
	options.setCallBack(&TetrisView::option,this);

	SongA.setTitle("Song A");
	SongB.setTitle("Song B");
	SongC.setTitle("Song C");
	options.setTitle("Options");

	newGameItem.setTitle("New Game");
	highScoreItem.setTitle("High Score");
	endItem.setTitle("End");
	newGameItem.setCallBack<TetrisView>(&TetrisView::newGame,this);
	highScoreItem.setCallBack<TetrisView>(&TetrisView::highScore,this);
	endItem.setCallBack<TetrisView>(&TetrisView::end,this);
	m.addItem(&newGameItem);
	m.addItem(&highScoreItem);
	m.addItem(&options);
	m.addItem(&endItem);

	Options.addItem(&SongA);
	Options.addItem(&SongB);
	Options.addItem(&SongC);
}

void TetrisView::end()
{
	done=1;
}

void TetrisView::newGame()
{
	change=true;
	song=selectedSong;
	run=1;
	tc.newGame();
}

void TetrisView::highScore()
{
	run=2;
}

void TetrisView::drawOptions()
{
	for(int i=0;i<Options.getMaxElements();i++)
	{
		if(Options.getSelected()==i)
		{
			c->putString(MENU_START_X,MENU_START_Y+i,Options.getTitle(i),Console::BG_RED|Console::BG_BRIGHTER|Console::FG_GREEN|Console::FG_BRIGHTER);
		}
		else
		{
			c->putString(MENU_START_X,MENU_START_Y+i,Options.getTitle(i),Console::BG_BLUE|Console::BG_BRIGHTER|Console::FG_GREEN|Console::FG_BRIGHTER);
		}
	}
}

void TetrisView::drawMenu()
{
	for(int i=0;i<m.getMaxElements();i++)
	{
		if(m.getSelected()==i)
		{
			c->putString(MENU_START_X,MENU_START_Y+i,m.getTitle(i),Console::BG_RED|Console::BG_BRIGHTER|Console::FG_GREEN|Console::FG_BRIGHTER);
		}
		else
		{
			c->putString(MENU_START_X,MENU_START_Y+i,m.getTitle(i),Console::BG_BLUE|Console::BG_BRIGHTER|Console::FG_GREEN|Console::FG_BRIGHTER);
		}
	}
}

void TetrisView::getMenuInput()
{
	if(c->isPushed(VK_DOWN))
	{
		m.move(1);
	}
	else if(c->isPushed(VK_UP))
	{
		m.move(-1);
	}
	else if(c->isPushed(VK_SPACE))
	{
		m.callBack(m.getSelected());
	}
}

void TetrisView::getOptionsInput()
{
	if(c->isPushed(VK_DOWN))
	{
		Options.move(1);
	}
	else if(c->isPushed(VK_UP))
	{
		Options.move(-1);
	}
	else if(c->isPushed(VK_SPACE))
	{
		Options.callBack(Options.getSelected());
	}
}

void TetrisView::mainloop()
{
	while(!done)
	{
		c->poll();
		getInput();
		if(run==1)
		{
			if(tc.getLoose())
			{
				run=2;
				drawLoose();
			}
			tc.timerProc();
		}
		draw();
		playSound();
	}
}

void TetrisView::drawField()
{
	for(int x=1;x<TetrisModel::FIELD_WIDTH;x++)
	{
		for(int y=0;y<TetrisModel::FIELD_HEIGHT;y++)
		{
			switch(field[y*TetrisModel::FIELD_WIDTH+x])
			{
			case 1:
				c->putChar(x+FIELD_START_X,y+FIELD_START_Y,' ',Console::BG_RED|Console::BG_BRIGHTER);
				break;
			case 2:
				c->putChar(x+FIELD_START_X,y+FIELD_START_Y,' ',Console::BG_RED|Console::BG_BLUE|Console::BG_BRIGHTER);
				break;
			case 3:
				c->putChar(x+FIELD_START_X,y+FIELD_START_Y,' ',Console::BG_GREEN|Console::BG_BRIGHTER);
				break;
			case 4:
				c->putChar(x+FIELD_START_X,y+FIELD_START_Y,' ',Console::BG_GREEN|Console::BG_BLUE|Console::BG_BRIGHTER);
				break;
			case 5:
				c->putChar(x+FIELD_START_X,y+FIELD_START_Y,' ',Console::BG_RED);
				break;
			case 6:
				c->putChar(x+FIELD_START_X,y+FIELD_START_Y,' ',Console::BG_RED|Console::BG_BLUE);
				break;
			case 7:
				c->putChar(x+FIELD_START_X,y+FIELD_START_Y,' ',Console::BG_GREEN);
				break;
			case 8:
				c->putChar(x+FIELD_START_X,y+FIELD_START_Y,' ',Console::BG_GREEN|Console::BG_BLUE);
				break;
			default:
				c->putChar(x+FIELD_START_X,y+FIELD_START_Y,' ',Console::BG_GREEN|Console::BG_RED|Console::BG_BLUE|Console::BG_BRIGHTER);
				break;
			}
		}
	}
	for(int x=0;x<TetrisModel::STONE_WIDTH;x++)
	{
		for(int y=0;y<TetrisModel::STONE_HEIGHT;y++)
		{
			if(nextStone[y*TetrisModel::STONE_WIDTH+x])
			{
				switch(tc.getNextColor())
				{
				case 1:
					c->putChar(x+PREVIEW_START_X,y+PREVIEW_START_Y,' ',Console::BG_RED|Console::BG_BRIGHTER);
					break;
				case 2:
					c->putChar(x+PREVIEW_START_X,y+PREVIEW_START_Y,' ',Console::BG_RED|Console::BG_BLUE|Console::BG_BRIGHTER);
					break;
				case 3:
					c->putChar(x+PREVIEW_START_X,y+PREVIEW_START_Y,' ',Console::BG_GREEN|Console::BG_BRIGHTER);
					break;
				case 4:
					c->putChar(x+PREVIEW_START_X,y+PREVIEW_START_Y,' ',Console::BG_GREEN|Console::BG_BLUE|Console::BG_BRIGHTER);
					break;
				case 5:
					c->putChar(x+PREVIEW_START_X,y+PREVIEW_START_Y,' ',Console::BG_RED);
					break;
				case 6:
					c->putChar(x+PREVIEW_START_X,y+PREVIEW_START_Y,' ',Console::BG_RED|Console::BG_BLUE);
					break;
				case 7:
					c->putChar(x+PREVIEW_START_X,y+PREVIEW_START_Y,' ',Console::BG_GREEN);
					break;
				case 8:
					c->putChar(x+PREVIEW_START_X,y+PREVIEW_START_Y,' ',Console::BG_GREEN|Console::BG_BLUE);
					break;
				default:
					c->putChar(x+PREVIEW_START_X,y+PREVIEW_START_Y,' ',Console::BG_GREEN|Console::BG_RED|Console::BG_BLUE|Console::BG_BRIGHTER);
					break;
				}
			}
			else
			{
				c->putChar(x+PREVIEW_START_X,y+PREVIEW_START_Y,' ',Console::BG_GREEN|Console::BG_RED|Console::BG_BLUE|Console::BG_BRIGHTER);
			}
			if(curStone[y*TetrisModel::STONE_WIDTH+x])
			{
				switch(tc.getCurColor())
				{
				case 1:
					c->putChar(tc.getCurX()+x+FIELD_START_X,tc.getCurY()+y+FIELD_START_Y,' ',Console::BG_RED|Console::BG_BRIGHTER);
					break;
				case 2:
					c->putChar(tc.getCurX()+x+FIELD_START_X,tc.getCurY()+y+FIELD_START_Y,' ',Console::BG_RED|Console::BG_BLUE|Console::BG_BRIGHTER);
					break;
				case 3:
					c->putChar(tc.getCurX()+x+FIELD_START_X,tc.getCurY()+y+FIELD_START_Y,' ',Console::BG_GREEN|Console::BG_BRIGHTER);
					break;
				case 4:
					c->putChar(tc.getCurX()+x+FIELD_START_X,tc.getCurY()+y+FIELD_START_Y,' ',Console::BG_GREEN|Console::BG_BLUE|Console::BG_BRIGHTER);
					break;
				case 5:
					c->putChar(tc.getCurX()+x+FIELD_START_X,tc.getCurY()+y+FIELD_START_Y,' ',Console::BG_RED);
					break;
				case 6:
					c->putChar(tc.getCurX()+x+FIELD_START_X,tc.getCurY()+y+FIELD_START_Y,' ',Console::BG_RED|Console::BG_BLUE);
					break;
				case 7:
					c->putChar(tc.getCurX()+x+FIELD_START_X,tc.getCurY()+y+FIELD_START_Y,' ',Console::BG_GREEN);
					break;
				case 8:
					c->putChar(tc.getCurX()+x+FIELD_START_X,tc.getCurY()+y+FIELD_START_Y,' ',Console::BG_GREEN|Console::BG_BLUE);
					break;
				default:
					c->putChar(tc.getCurX()+x+FIELD_START_X,tc.getCurY()+y+FIELD_START_Y,' ',Console::BG_GREEN|Console::BG_RED|Console::BG_BLUE|Console::BG_BRIGHTER);
					break;
				}
			}
			else
			{
				int color=c->getColorxy(tc.getCurX()+x+FIELD_START_X,tc.getCurY()+y+FIELD_START_Y);
				c->putChar(tc.getCurX()+x+FIELD_START_X,tc.getCurY()+y+FIELD_START_Y,' ',color);
			}
		}
	}
	char tmp[100];
	sprintf(tmp,"Points: %i",tc.getPoints());
	c->putString(STATS_START_X,POINTS_START_Y,tmp,c->getColorxy(STATS_START_X,POINTS_START_Y));
	sprintf(tmp,"Lines:  %i",tc.getLines());
	c->putString(STATS_START_X,LINES_START_Y,tmp,c->getColorxy(STATS_START_X,LINES_START_Y));
	sprintf(tmp,"Level:  %i",tc.getLevel());
	c->putString(STATS_START_X,LEVEL_START_Y,tmp,c->getColorxy(STATS_START_X,LEVEL_START_Y));
}

void TetrisView::drawLoose()
{
	c->clrscr(' ',0);
	c->draw();
	nameInput();
	change=true;
	song=2;
}

void TetrisView::draw()
{
	c->clrscr(' ',Console::BG_BLUE|Console::BG_BRIGHTER);
	if(run==1)
	{
		drawField();
	}
	else if(run==2)
	{
		drawHighScore();
	}
	else if(run==3)
	{
		drawOptions();
	}
	else
	{
		drawMenu();
	}
	c->draw();
}

void TetrisView::getInput()
{
	if(c->isPushed(VK_ESCAPE))
	{
		change=true;
		if(run)
		{
			song=2;
			tc.stop();
			run=0;
		}
		else
		{
			song=selectedSong;
			tc.resume();
			run=1;
		}
	}
	if(run==1)
	{
		getTetrisInput();
	}
	else if(run==2)
	{
		if(c->isPushed(VK_SPACE))
		{
			run=0;
		}
	}
	else if(run==3)
	{
		getOptionsInput();
	}
	else
	{
		getMenuInput();
	}
}

void TetrisView::getTetrisInput()
{
	if(c->isPushed(VK_SPACE))
	{
		tc.rotate();
	}
	else if(c->isPushed(VK_LEFT))
	{
		tc.move(-1,0);
	}
	else if(c->isPushed(VK_RIGHT))
	{
		tc.move(1,0);
	}
	else if(c->isPushed(VK_DOWN))
	{
		tc.move(0,1);
	}
}

void TetrisView::nameInput()
{
	char name[100];
	cout<<"Enter Name: ";
	cin>>name;
	name[3]='\0';
	Score s;
	s.setName(name);
	s.setScore(tc.getPoints());
	score->addScore(s);
}

void TetrisView::drawHighScore()
{
	c->putString(15,9,"Name    Points",Console::FG_RED|Console::BG_BLUE|Console::FG_BRIGHTER|Console::BG_BRIGHTER);
	if(score->getNumScores()==0)
	{
		return;
	}
	for(int i=0;i<10;i++)
	{
		char sc[100];
		Score s=score->getScore(i);
		sprintf(sc,"%s %6i",s.getName(),s.getScore());
		c->putString(15,10+i,sc,Console::FG_RED|Console::BG_BLUE|Console::FG_BRIGHTER|Console::BG_BRIGHTER);
		if(i+1>=score->getNumScores())
		{
			break;
		}
	}
}

void TetrisView::readScores()
{
	ifstream in("scores.sc",ifstream::in);
	if(!in||in.eof())
	{
		in.close();
		return;
	}
	int sc;
	char name[100];
	for(int i=0;i<10;i++)
	{
		if(in.eof())
		{
			break;
		}
		Score s;
		in>>name>>sc;
		s.setName(name);
		s.setScore(sc);
		score->addScore(s);
	}
	in.close();
}

void TetrisView::writeScores()
{
	ofstream out("scores.sc",ifstream::out|ifstream::binary);
	for(int i=0;i<10;i++)
	{
		if(i+1>score->getNumScores())
		{
			break;
		}
		out<<score->getScore(i).getName()<<" "<<score->getScore(i).getScore();
	}
	out.close();
}

void TetrisView::initSound()
{
	FSOUND_Init(44100,32,0);
	tetrisa=FMUSIC_LoadSong("tetrisa.mid");
	tetrisb=FMUSIC_LoadSong("tetrisb.mid");
	tetrisc=FMUSIC_LoadSong("tetrisc.mid");
	tetris_theme=FMUSIC_LoadSong("tetris_menu.mid");
}

void TetrisView::deinitSound()
{
	FMUSIC_FreeSong(tetrisa);
	FMUSIC_FreeSong(tetrisc);
	FMUSIC_FreeSong(tetris_theme);
}

void TetrisView::playSound()
{
	if(change)
	{
		if(song==0)
		{
			FMUSIC_StopAllSongs();
			FMUSIC_PlaySong(tetrisa);
		}
		else if(song==1)
		{
			FMUSIC_StopAllSongs();
			FMUSIC_PlaySong(tetrisb);
		}
		else if(song==3)
		{
			FMUSIC_StopAllSongs();
			FMUSIC_PlaySong(tetrisc);
		}
		else if(song==2)
		{
			FMUSIC_StopAllSongs();
			FMUSIC_PlaySong(tetris_theme);
		}
		change=false;
	}
}

void TetrisView::selectSongA()
{
	selectedSong=0;
}

void TetrisView::selectSongB()
{
	selectedSong=1;
}

void TetrisView::selectSongC()
{
	selectedSong=3;
}

void TetrisView::option()
{
	run=3;
}
