/*    This file is part of ANSITRIS.

    ANSITRIS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ANSITRIS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ANSITRIS.  If not, see <http://www.gnu.org/licenses/>.*/
#pragma once
#include "MenuItem.h"
#include "TetrisControl.h"
#include "Console.h"
#include "Menu.h"
#include "Highscore.h"
#include <fmod.h>

class TetrisView
{
public:
	TetrisView(void);
	~TetrisView(void);
	void mainloop();
private:
	static enum
	{
		PREVIEW_START_X=3,
		PREVIEW_START_Y=3,
		FIELD_START_X=10,
		FIELD_START_Y=1,
		STATS_START_X=25,
		POINTS_START_Y=10,
		LINES_START_Y=11,
		LEVEL_START_Y=12,
		MENU_START_X=10,
		MENU_START_Y=10
	};
	int done;
	int run;
	void draw();
	void drawField();
	void drawLoose();
	void drawMenu();
	void getInput();
	void getTetrisInput();
	void getMenuInput();
	void createMenu();
	void end();
	void newGame();
	void highScore();
	void nameInput();
	void initSound();
	void deinitSound();
	void playSound();
	void selectSongA();
	void selectSongB();
	void selectSongC();
	void option();
	void drawOptions();
	void getOptionsInput();
	int *field;
	int *curStone;
	int *nextStone;
	void drawHighScore();
	void readScores();
	void writeScores();
	TetrisControl tc;
	int song;
	int selectedSong;
	bool change;
	Console *c;
	Menu m;
	Menu Options;
	MenuItem SongA;
	MenuItem SongB;
	MenuItem SongC;
	MenuItem options;
	MenuItem endItem;
	MenuItem newGameItem;
	MenuItem highScoreItem;
	Highscore* score;
	FMUSIC_MODULE* tetrisa;
	FMUSIC_MODULE* tetrisc;
	FMUSIC_MODULE* tetrisb;
	FMUSIC_MODULE* tetris_theme;
};
